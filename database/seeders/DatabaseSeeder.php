<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Category, Article};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::factory(10)->create()->each(function($category) {
            rand(0, 1) ?: Article::factory(rand(1, 3))->for($category)->create();
        });
    }
}
