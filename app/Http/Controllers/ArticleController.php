<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Category, Article};

class ArticleController extends Controller
{
    /**
     * Отдает пять последних статей для определенной категории
     * @param Category $category
     */
    public function showLastFiveByCategory(Request $request)
    {
        // обязательный параметр category
        if(!isset($request->category))
            return response()->json(['Error' => 'Category is required parameter'], 400);

        // проверяем существует ли категория
        if(!$category = Category::find($request->category))
            return response()->json(['Error' => 'Category not found'], 404);

        $articles = $category->articles()->latest()->take(5)->get();
        // возвращаем статьи, либо "end"
        return count($articles) ? $articles : 'end';
    }
}
